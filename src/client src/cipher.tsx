const chunkArr = (array: any, elementsPerChunk: number): any[] => {
  const result = array.reduce(
    (resultArr: any[][], item: number | string, index: number) => {
      const chunkIndex = Math.floor(index / elementsPerChunk);
      if (!resultArr[chunkIndex]) {
        resultArr[chunkIndex] = [];
      }
      resultArr[chunkIndex].push(item);
      return resultArr;
    },
    []
  );
  return result;
};

const convertDecToHex = (decimal: number): string => {
  return decimal.toString(16);
};

const convertHexToDec = (hexadecimal: string): number => {
  return parseInt(hexadecimal, 16);
};

const charsArrToHexArr = (array: number[]): string[] => {
  return array.map((char) => convertDecToHex(char));
};

const convertCharToCode = (character: string): number => {
  return character.charCodeAt(0);
};

const convertCodeToChar = (character: number): string => {
  return String.fromCharCode(character);
};

const convertEachHexElementToDec = (array: string[]): number[] => {
  return array.map((element) => convertHexToDec(element));
};

const textStringToCharsArr = (string: string): number[] => {
  const textCharsArr = string.split("");
  return textCharsArr.map((character) => convertCharToCode(character));
};

const equalizeLengthOfElements = (array: string[], size: number): string[] => {
  const result = array.map((element) => {
    let s = element + "";
    while (s.length < size) s = "0" + s;
    return s;
  });
  return result;
};

const multiplyChunk = (
  array: number[],
  multiplicandsArray: number[]
): number[] => {
  return array.map((element, index) => element * multiplicandsArray[index]);
};

const cipherChunk = (array: number[], keyArr: number[]): string => {
  const multipliedChunk = multiplyChunk(array, keyArr);
  const hexArr = charsArrToHexArr(multipliedChunk);
  const hexArrWithEvenElements = equalizeLengthOfElements(hexArr, 6);
  return hexArrWithEvenElements.join("");
};

const cipherChunkedCharsArr = (array: number[][], key: string): string[] => {
  const keyCharsArr = textStringToCharsArr(key);
  return array.map((chunk) => cipherChunk(chunk, keyCharsArr));
};

const chunkString = (
  string: string,
  charsPerChunk: number
): RegExpMatchArray | null => {
  const reg = new RegExp(`.{1,${charsPerChunk}}`, "g");
  console.log(string.match(reg));
  return string.match(reg);
};

const chunkedHexArrToChunkedDecArr = (array: string[][]): number[][] => {
  return array.map((array) => convertEachHexElementToDec(array));
};

const divideEachChunk = (
  array: number[],
  divisorsArray: number[]
): number[] => {
  return array.map((element, index) => element / divisorsArray[index]);
};

const decipherChunkedCharsArr = (
  array: number[][],
  key: string
): number[][] => {
  const keyCharsArr = textStringToCharsArr(key);
  return array.map((chunk) => divideEachChunk(chunk, keyCharsArr));
};

const joinChunkedArray = (array: number[][]): number[] => {
  return ([] as number[]).concat(...array);
};

const decCharsArrtoCharsArr = (array: number[]): string[] => {
  return array.map((decChar) => convertCodeToChar(decChar));
};

const encode = (text: string, key: string): string => {
  const inputCharsArr = textStringToCharsArr(text);
  const chunkedInputCharsArr = chunkArr(inputCharsArr, key.length);
  const cipheredChunkedInputCharsArr = cipherChunkedCharsArr(
    chunkedInputCharsArr,
    key
  );
  const cipheredText = cipheredChunkedInputCharsArr.join("");
  return cipheredText;
};

const decode = (text: string, key: string): string => {
  const hexCharsArr = chunkString(text, 6);
  const chunkedHexCharsArr = chunkArr(hexCharsArr, key.length);
  const chunkedDecCharsArr = chunkedHexArrToChunkedDecArr(chunkedHexCharsArr);
  const decipheredChunkedDecCharsArr = decipherChunkedCharsArr(
    chunkedDecCharsArr,
    key
  );
  const decCharsArr = joinChunkedArray(decipheredChunkedDecCharsArr);
  const charsArr = decCharsArrtoCharsArr(decCharsArr);
  const string = charsArr.join("");
  return string;
};

export { encode, decode };
