import md5 from "md5";
import users from "./users";

const login = (url: string, setIsAuthenticated: any, history: any) => {
  setIsAuthenticated(true);
  history.push(url);
};

const logout = (url: string, setIsAuthenticated: any, history: any) => {
  setIsAuthenticated(false);
  history.push(url);
};

const authenticate = (userLogin: string, password: string, url: string, setIsAuthenticated: boolean, history: any) => {
  const authenticate = users.find(
    (user) => user.hashedPassword === md5(password) && user.login === userLogin
  );
  if (authenticate) {
    login(url, setIsAuthenticated, history);
  }
};

export { login, logout, authenticate };
