import { encode, decode } from "./client src/cipher";

test("cipher encoding", () => {
  const text = "Jechał Michał Koń Mu Zdychał!";
  const key = "Okoń";
  const result =
    "0016d6002a37002aed0083a0001def008696000de0006174002067002961002d18007ac400635e000d60002085008c7c0063fc000d600021630094140009e000259e002b5c009924001e8d002b78002a0f019788000a2f";
  expect(encode(text, key)).toBe(result);
});

test("cipher encoding", () => {
  const text = "we		dze";
  const key = "deca";
  const result = "0026e800393100346c002d7800044100044a00348000365b00320a";
  expect(encode(text, key)).toBe(result);
});

test("cipher decoding", () => {
  const text =
    "0016d6002a37002aed0083a0001def008696000de0006174002067002961002d18007ac400635e000d60002085008c7c0063fc000d600021630094140009e000259e002b5c009924001e8d002b78002a0f019788000a2f";
  const key = "Okoń";
  const result = "Jechał Michał Koń Mu Zdychał!";
  expect(decode(text, key)).toBe(result);
});

test("cipher decoding", () => {
  const text = "0026e800393100346c002d7800044100044a00348000365b00320a";
  const key = "deca";
  const result = "we		dze";
  expect(decode(text, key)).toBe(result);
});